
//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <SI_EFM8BB1_Register_Enums.h>                  // SFR declarations
#include "InitDevice.h"
#include "EFM8BB1_SMBus_SlaveMultibyte.h"
#include <math.h>

//macros
#define LED P1_B3
#define	LEFT_SENSOR P0_B7
#define MIDDLE_SENSOR P1_B0
#define RIGHT_SENSOR P1_B1
#define ON 1
#define OFF 0
#define LEFT 1											//string position (viewed from pianist)
#define MIDDLE 2
#define RIGHT 3
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define FLIPBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define SETBITMASK(x,y) (x |= (y))
#define CLEARBITMASK(x,y) (x &= (~y))
#define FLIPBITMASK(x,y) (x ^= (y))
#define CHECKBITMASK(x,y) (x & (y))

//functions
void Delay_ms(uint16_t ms);
void SMBus_Check(void);

//Biao Fu variables?
volatile uint16_t timer_val_0, timer_val_1;
volatile float frq,frq_start,frq_end;
volatile uint8_t sw;
volatile uint8_t string_no = LEFT;	//  <----------set string (left 1, middle 2, right 3, off 0)
volatile uint16_t period = 0;
volatile uint8_t cntr = 0;
uint8_t test_mode = 0;
uint8_t enable = 0;
bit success = 0;
uint8_t st_cntr = 0;
uint8_t scan_cntr = 0;

//Don variables
float sysclock = 20e06;												//system clock frequency (adjusted to calibrate)
volatile uint16_t correct_period;									//correct value of period for 166 Hz (test string)
extern volatile float float_period = 0;								//temporary float value for correct_period
volatile float mingood_float;										//temporary float variable (converted to integer mingood)
volatile float N = 31;					//  <----------set note number (A0 = 0)
volatile uint16_t susmode = 0;										//0 = goose mode; 1 = phase delay capture mode; 2 = natural sustain mode
volatile uint16_t goodcount = 0;									//no. of consecutive good readings from string
volatile uint32_t mingood;											//minimum allowable good value (+ 50 cents)
volatile uint16_t maxgood;											//maximum allowable good value (- 50 cents)
volatile uint16_t phase_delay = 0;									//phase shift between sensor input and coil output
volatile uint16_t goosetime;										//correct_period adjusted for PCA goose driver
volatile uint32_t total_period = 0;									//sum for averaging period
volatile uint16_t period_out = 0;									//average period for export to master
volatile uint8_t d = 1;												//clock divisor
volatile uint8_t waves = 1;											//measure multiple waves for high treble
volatile uint8_t avcount = 0;										//counter for averaging
volatile uint16_t period_sample = 0;								//period counted by timer1
volatile uint8_t wavecount = 1;										//count of waves for multiple-wave measurement, 1-4
volatile uint16_t debug = 0;
volatile uint8_t avsamples = 1;										//number of samples to average and acceptable number of good cycles (smaller for low notes)
volatile bit crossover_mode = 0;									//special mode for sustainers placed in odd positions on strings
volatile float left_factor = 0;										//adjustments for phase delays (-0.48 > x > +0.48)
volatile float middle_factor = 0;
volatile float right_factor = 0;
volatile uint8_t calcount = 0;										//count of bad period measurements to trigger phase adjustment
volatile uint8_t coil_power = 100;									//PWM duty cycle for coil current control, 100 = full current
volatile uint8_t sensor_brightness = 100;							//PWM duty cycle for sensor IRLED brightness
volatile uint8_t aux_brightness = 100;								//PWM duty cycle for aux IRLED brightness

int main (void)
{
	enter_DefaultMode_from_RESET(); 								//call hardware initialization routine

	//note presets
	if(N >= 43)														//notes above Eb4, full system clock speed, goose and measure
	{
		SETBITMASK(CKCON0, CKCON0_T1M__BMASK);						//full speed clock for timer1
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);					//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK);						//full speed clock for PCA goose timer
		d = 1;
		avsamples = 150;											//average low treble over 150 samples
		coil_power = 100;
		sensor_brightness = 100;
		aux_brightness = 100;
	}
	if((N < 43 && N >= 19) || N > 100)								//midrange notes between D2 and Eb4 (or crossover notes), divide system clock by 4
	{
		CLEARBITMASK(CKCON0, CKCON0_T1M__BMASK);					//timer1 prescaled
		CLEARBITMASK(CKCON0, CKCON0_T0M__BMASK);					//timer0 prescaled
		CLEARBITMASK(CKCON0, CKCON0_SCA__FMASK);					//clear SCA (timer1 clock select)
		SETBITMASK(CKCON0, CKCON0_SCA__SYSCLK_DIV_4);				//divide clock by 4 for timer1 & 2
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);					//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK_DIV_4);				//divide clock by 4 for PCA goose timer
		d = 4;
		avsamples = 50;												//average midrange over 50 samples
		coil_power = 100;
		sensor_brightness = 100;
		aux_brightness = 100;
	}
	if(N < 19)														//low notes below D2, divide system clock by 12
	{
		CLEARBITMASK(CKCON0, CKCON0_T1M__BMASK);					//timer1 prescaled
		CLEARBITMASK(CKCON0, CKCON0_T0M__BMASK);					//timer0 prescaled
		CLEARBITMASK(CKCON0, CKCON0_SCA__FMASK);					//clear SCA (timer1 select)
		SETBITMASK(CKCON0, CKCON0_SCA__SYSCLK_DIV_12);				//divide clock by 12 for timer1 & 2
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);					//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK_DIV_12);				//divide clock by 12 for PCA goose timer
		d = 12;
		avsamples = 15;												//average bass over 15 samples
		coil_power = 100;
		sensor_brightness = 100;
		aux_brightness = 100;
	}
	if(N > 100)														//convert crossover address to normal address and set mode
	{
		N -= 100;
		crossover_mode = 1;
	}

	//initial calculations and presets
	float_period = (sysclock / d) / 27.5 / pow(2, N/12);			//calculate correct period for string based on note N (clock divided by d)
	correct_period = floor(float_period + 0.5);						//correct_period round to integer
	if(N > 63)														//measure 4 complete waves for high treble (more accuracy)
	{
		waves = 4;
		correct_period = floor(4 * float_period + 0.5);				//adjust correct period for four waves
		avsamples = 5;												//average high treble over 500 samples
	}
	if(crossover_mode)												//read second harmonic for crossover notes (2f)
		correct_period /= 2;
	maxgood = floor(correct_period * 1.03 + 0.5);					//range for valid periods +/- 3% (50 cents)
	mingood = floor(correct_period * .97 + 0.5);
	goosetime = 0xFFFF - (correct_period / 2);						//proper period value for goose (1/2 period for toggle, count up to 0xFFFF overflow interrupt)
	if(waves == 4)
		goosetime = 0xFFFF - (correct_period / 8);					//adjust goose frequency for multiple-wave measurement
	PCA0H = goosetime << 8;											//preset PCA goose driver
	PCA0L = goosetime;
	PCA0CN0_CF = 0;													//clear PCA overflow flag
	SETBITMASK(PCA0CN0, PCA0CN0_CR__BMASK);							//enable PCA timer (start goose)
	SETBITMASK(TCON, TCON_TR1__BMASK);								//start T1 (begin timing string periods)
	CLEARBITMASK(IE, IE_ET1__BMASK);								//disable T1 overflow interrupt during goose
	CLEARBITMASK(IE, IE_ET0__BMASK);								//disable T0 overflow interrupt during goose

	//main program loop
	while(1)
	{
		if(goodcount > avsamples && susmode == 0)
		{
			susmode = 1;											//natural sustain mode
			SETBITMASK(IE, IE_ET1__BMASK);							//enable timer1 overflow interrupt for signal loss detection
			SETBITMASK(IE, IE_ET0__BMASK);							//enable timer0 overflow interrupt (phase delay)
			PCA0CN0_CR = 0;											//disable PCA counter (turn off goose)
			goodcount = 0;
		}
		LED = susmode;												//LED is on when in natural sustain
		if(avcount == avsamples && susmode)							//averaging routine
		{
			period = total_period / avsamples;						//average period readings
			avcount = 0;											//zero count for next average
			total_period = 0;										//zero total for next average
		}

		//string setups based on string selected by master
		if(string_no == LEFT) 										// P0.7 Left string setup
				{
					P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED	//read appropriate sensor
							| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
							| P0MASK_B6__IGNORED | P0MASK_B7__COMPARED;

					P1MASK = P1MASK_B0__IGNORED | P1MASK_B1__IGNORED | P1MASK_B2__IGNORED
							| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
							| P1MASK_B6__IGNORED;
					LEFT_IR = ON;
					MIDDLE_IR = OFF;
					RIGHT_IR = OFF;
					if(left_factor < 0)
						phase_delay = 0xFFFF - floor(float_period * (left_factor + 0.5));	//phase delay for timer0, left string
						else
							phase_delay = 0xFFFF - floor(float_period * left_factor);
				}
		else if(string_no == MIDDLE) 								// P1.0 Middle string setup
				{
					P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED
							| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
							| P0MASK_B6__IGNORED | P0MASK_B7__IGNORED;

					P1MASK = P1MASK_B0__COMPARED | P1MASK_B1__IGNORED | P1MASK_B2__IGNORED
							| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
							| P1MASK_B6__IGNORED;
					LEFT_IR = OFF;
					MIDDLE_IR = ON;
					RIGHT_IR = OFF;
					if(middle_factor < 0)
						phase_delay = 0xFFFF - floor(float_period * (middle_factor + 0.5));	//phase delay for timer0, middle string
							else
								phase_delay = 0xFFFF - floor(float_period * middle_factor);
				}
		else if(string_no == RIGHT)									// P1.1 Right string setup
				{
					P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED
							| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
							| P0MASK_B6__IGNORED | P0MASK_B7__IGNORED;

					P1MASK = P1MASK_B0__IGNORED | P1MASK_B1__COMPARED | P1MASK_B2__IGNORED
							| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
							| P1MASK_B6__IGNORED;
					LEFT_IR = OFF;
					MIDDLE_IR = OFF;
					RIGHT_IR = ON;
					if(right_factor < 0)
						phase_delay = 0xFFFF - floor(float_period * (right_factor + 0.5));	//phase delay for timer0, right string
							else
								phase_delay = 0xFFFF - floor(float_period * right_factor);
				}

		else if (string_no == OFF)									//shut down sustainer, turn off coil and all LED's
				{
					P0MASK = 0;
					P1MASK = 0;
					LEFT_IR = OFF;
					MIDDLE_IR = OFF;
					RIGHT_IR = OFF;
					COIL_DRIVER = OFF;
					susmode = 0;
					LED = OFF;
				}

		//phase delay auto-adjust
		if(calcount > 5)											//increment phase delay guess for every 5 bad period readings
		{
			calcount = 0;
			susmode = 0;
			if(LEFT_IR)												//left string being sustained
			{
				left_factor += 0.05;								//increment
				if(left_factor > 0.45)								//start over at -90 if full range exceeded (+90 deg)
					left_factor = -0.45;							//(-90 deg)
			}
			if(MIDDLE_IR)
			{
				middle_factor += 0.05;
				if(middle_factor > 0.45)
					middle_factor = -0.45;
			}
			if(RIGHT_IR)
			{
				right_factor += 0.05;
				if(right_factor > 0.45)
					right_factor = -0.45;
			}
		}
	}


//ORIGINAL CODE
	/*while(0)
	{
		WDTCN =0xA5;

		EIE1 = EIE1_EADC0__DISABLED | EIE1_EWADC0__DISABLED | EIE1_ECP0__DISABLED
				| EIE1_ECP1__DISABLED | EIE1_EMAT__DISABLED | EIE1_EPCA0__DISABLED
				| EIE1_ESMB0__ENABLED | EIE1_ET3__ENABLED;
		IE = IE_EA__ENABLED | IE_EX0__DISABLED | IE_EX1__DISABLED
				| IE_ESPI0__DISABLED | IE_ET0__DISABLED | IE_ET1__DISABLED
				| IE_ET2__DISABLED | IE_ES0__DISABLED;

		LEFT_IR = 0; 			// Left IR sensor driver
		MIDDLE_IR = 0; 			// Middle IR sensor driver
		RIGHT_IR = 0; 			// Right IR sensor driver
		COIL_DRIVER = 0;		// Turn off the coil

		SMB_DATA_OUT[0] = 0; 	// Status: Standby
		SMB_DATA_OUT[1] = 0;
		SMB_DATA_OUT[2] = 0;

		sw = 0;
		TCON_TR0 = 0;
		TCON_TR1 = 0;
		PCA0CN0_CR = 0;

		//-------------------------------------------------------------------------------------------------
		// Waiting for setting and enable command
		//-------------------------------------------------------------------------------------------------
		test_mode = 0;
		enable = 0;
		while(enable == 0)
		{
			WDTCN =0xA5;
			COIL_DRIVER = 0;

			SMBus_Check(); // Check SMBus

			if((enable == 1) & (test_mode == 0x02)) // All IR sensors on
			{
				LEFT_IR = 1; 			// Left IR sensor driver
				MIDDLE_IR = 1; 			// Middle IR sensor driver
				RIGHT_IR = 1; 			// Right IR sensor driver
				COIL_DRIVER = 0;

				exit = 0;
				while(exit == 0)
				{
					//-------------------------------------------------------------------------------------------------
					// Check SMBus
					//-------------------------------------------------------------------------------------------------
					SMBus_Check();
					if(enable == 0)
					{
						LEFT_IR = 0; 			// Left IR sensor driver
						MIDDLE_IR = 0; 			// Middle IR sensor driver
						RIGHT_IR = 0; 			// Right IR sensor driver
						exit = 1;
					}
					//-------------------------------------------------------------------------------------------------
					// End of Check SMBus
					//-------------------------------------------------------------------------------------------------
				}
			}
		}
		//-------------------------------------------------------------------------------------------------
		// End of Waiting for setting and enable command
		//-------------------------------------------------------------------------------------------------*/



		/*temp_0 = (uint16_t)(1/frq/0.1*1e6*0.994);
		if(temp_0 > 65535)
			temp_0 = 65535;
		timer_val_0 = 65535 - temp_0;
		PCA0H = timer_val_0 >> 8;
		PCA0L = timer_val_0;

		temp_1 = temp_0 >> 2;
		timer_val_1 = 65535 - temp_1;

		sw = 0;
		PCA0CN0_CR = 1;
		LED = 0;

		EIE1 = EIE1_EADC0__DISABLED | EIE1_EWADC0__DISABLED | EIE1_ECP0__DISABLED
				| EIE1_ECP1__DISABLED | EIE1_EMAT__ENABLED | EIE1_EPCA0__ENABLED
				| EIE1_ESMB0__ENABLED | EIE1_ET3__ENABLED;

		IE = IE_EA__ENABLED | IE_EX0__DISABLED | IE_EX1__DISABLED
				| IE_ESPI0__DISABLED | IE_ET0__ENABLED | IE_ET1__DISABLED
				| IE_ET2__DISABLED | IE_ES0__DISABLED;

		SMB_DATA_OUT[0] = 0x10 | string_no; // Status: Artificial

		IE_EA = 0;
		period = 0;
		IE_EA = 1;

		TH1 = 0;
		TL1 = 0;
		TCON_TR1 = 1;
		Delay_ms(500);

		//while(1){}

		/*
		//-------------------------------------------------------------------------------------------------
		// Fixed artificial frequency
		//-------------------------------------------------------------------------------------------------
		for(i = 0; i < 60; i++) // Delay 3s
		{
			Delay_ms(50);

			SMBus_Check();
			if(enable == 0)
			{
				i = 60;
				PCA0CN0_CR = 0;
			}
		}
		//-------------------------------------------------------------------------------------------------
		// End of Fixed artificial frequency
		//-------------------------------------------------------------------------------------------------



		//-------------------------------------------------------------------------------------------------
		// Frequency scanning
		//-------------------------------------------------------------------------------------------------
		st_cntr = 0;
		scan_cntr = 0;
		success = 0;
		exit = 0;
		while(exit == 0)
		{
			WDTCN =0xA5;
			Delay_ms(50);

			if(test_mode == 1)
			{
				frq = frq - 0.5;
				if(frq < frq_end)
					frq = frq_start;

				temp_0 = (uint16_t)(1/frq/0.1*1e6*0.994);
				if(temp_0 > 65535)
					temp_0 = 65535;
				timer_val_0 = 65535 - temp_0;
				PCA0H = timer_val_0 >> 8;
				PCA0L = timer_val_0;

				temp_1 = temp_0 >> 2;
				timer_val_1 = 65535 - temp_1;
			}
			else
			{
				IE_EA = 0;
				temp = period;
				period = 0;
				IE_EA = 1;
				f = 20.0 * 1e6 / (float)(temp+1);

				if(fabs(f-frq)/frq > 0.1)
				{
					frq = frq - 0.5;
					if(frq < frq_end)
					{
						frq = frq_start;
						scan_cntr++;
						if(scan_cntr > 2)
						{
							success = 0;
							exit = 1;
						}
					}

					temp_0 = (uint16_t)(1/frq/0.1*1e6*0.994);
					if(temp_0 > 65535)
						temp_0 = 65535;
					timer_val_0 = 65535 - temp_0;
					PCA0H = timer_val_0 >> 8;
					PCA0L = timer_val_0;

					temp_1 = temp_0 >> 2;
					timer_val_1 = 65535 - temp_1;

					st_cntr = 0;
				}
				else
				{
					st_cntr++;
					if(st_cntr > 20)
					{
						st_cntr = 0;
						success = 1;
						exit = 1;
					}
				}
			}

			//-------------------------------------------------------------------------------------------------
			// Check SMBus
			//-------------------------------------------------------------------------------------------------
			SMBus_Check();
			if(enable == 0)
			{
				exit = 1;
				COIL_DRIVER = 0;
			}
			//-------------------------------------------------------------------------------------------------
			// End of Check SMBus
			//-------------------------------------------------------------------------------------------------

		}
		//-------------------------------------------------------------------------------------------------
		// End of Frequency scanning
		//-------------------------------------------------------------------------------------------------

		COIL_DRIVER = 0;


		//-------------------------------------------------------------------------------------------------
		// Sustaining
		//-------------------------------------------------------------------------------------------------
		if((enable == 1) & (test_mode == 0) & (success == 1))
		{
			PCA0CN0_CR = 0;
			PCA0H = timer_val_1 >> 8;
			PCA0L = timer_val_1;
			PCA0CN0_CF = 0;

			sw = 1;
			LED = 1;

			for(i = 0; i < 20; i++) // Delay 1s
			{
				Delay_ms(50);

				SMBus_Check();
				if(enable == 0)
				{
					i = 20;
					COIL_DRIVER = 0;
				}
			}

			if(enable)
			{
				TH1 = 0;
				TL1 = 0;
				TCON_TR1 = 1;

				SMB_DATA_OUT[0] = 0x20 | string_no;  // Status: sustaining

				while(enable)
				{
					WDTCN =0xA5;

					period_sum = 0;
					for(i = 0; i < 16; i++)
					{
						Delay_ms(50);
						IE_EA = 0;
						temp = period;
						period = 0;
						IE_EA = 1;
						period_sum += temp;

						SMBus_Check();
						if(enable == 0)
						{
							i = 16;
							COIL_DRIVER = 0;
						}
					}

					if(enable)
					{
						f = 20.0 * 1e6 / (float)(period_sum+1) * 16 * 10;
						IE_EA = 0;
						SMB_DATA_OUT[1] = (uint16_t)f >> 8;
						SMB_DATA_OUT[2] = (uint16_t)f;
						IE_EA = 1;
					}
				}
			}
			//-------------------------------------------------------------------------------------------------
			// End of Sustaining
			//-------------------------------------------------------------------------------------------------
		}


		//-------------------------------------------------------------------------------------------------
		// Sustaining failure
		//-------------------------------------------------------------------------------------------------
		if((enable == 1) & (test_mode == 0) & (success == 0))
		{
			IE_EA = 0;
			SMB_DATA_OUT[0] = 0x30 | string_no; // Status: Artificial
			SMB_DATA_OUT[1] = 0x00;
			SMB_DATA_OUT[2] = 0x00;
			IE_EA = 1;

			exit = 0;
			while(exit == 0)
			{
				//-------------------------------------------------------------------------------------------------
				// Check SMBus
				//-------------------------------------------------------------------------------------------------
				SMBus_Check();
				if(enable == 0)
				{
					exit = 1;
					COIL_DRIVER = 0;
				}
				//-------------------------------------------------------------------------------------------------
				// End of Check SMBus
				//-------------------------------------------------------------------------------------------------
			}
		}
		//-------------------------------------------------------------------------------------------------
		// End of Sustaining failure
		//-------------------------------------------------------------------------------------------------
	}*/
		// $[Generated Run-time code]
		// [Generated Run-time code]$
}

void SMBus_Check(void)
{
	if(DATA_READY == 1)
	{
		DATA_READY = 0;

		switch(SMB_DATA_IN[0])
		{
		case 0:
			break;

		case 1:
			break;

		case 2:
			string_no = SMB_DATA_IN[1];
			if(string_no > 2)
				string_no = 2;

			test_mode = (SMB_DATA_IN[2] & 0xF0) >> 4;
			enable = SMB_DATA_IN[2] & 0x0F;
			break;

		case 3:
			break;

		case 4:
			break;

		case 5: // f = 27.5*2^(N/12), N=0,1,...87
			//frq_start = (float)((SMB_DATA_IN[1] << 8) + SMB_DATA_IN[2]) / 10.0;
			frq = frq_start;
			frq_end = frq_start * 0.95;
			break;

		default:
			break;
		}
	}
}

/*void Delay_ms(uint16_t ms)
{
	uint16_t i,j;

	for(i = 0; i < ms; i++)
	{
		for(j = 0; j < 2180; j++){}
	}
	WDTCN =0xA5;
}*/
