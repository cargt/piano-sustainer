
//-----------------------------------------------------------------------------
// Includes
//-----------------------------------------------------------------------------
#include <SI_EFM8BB1_Register_Enums.h>              // SFR declarations
#include "InitDevice.h"
#include "EFM8BB1_SMBus_SlaveMultibyte.h"
#include <math.h>

#define DEFAULT_NOTE	39							// string number (hard coded for each sustainer, A0 = 0)
#define DEFAULT_STRING	LEFT

#define GLOBAL_ADDRESS	250							// writes to global address set all sustainers to standby mode
#define	LEFT_SENSOR		P0_B7
#define MIDDLE_SENSOR	P1_B0
#define RIGHT_SENSOR	P1_B1
#define LED				P1_B3
#define LEFT_IR 		P0_B2
#define MIDDLE_IR 		P0_B1
#define RIGHT_IR 		P1_B6
#define LED 			P1_B3
#define COIL_D1			P0_B0
#define COIL_D2			P0_B4
#define OD_TEST1		P0_B6
#define OD_TEST2		P1_B2
#define ON 				1
#define OFF 			0
#define LEFT 			1							//string position (viewed from pianist)
#define MIDDLE 			2
#define RIGHT 			3
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT))
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define FLIPBIT(ADDRESS,BIT) (ADDRESS ^= (1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))
#define SETBITMASK(x,y) (x |= (y))
#define CLEARBITMASK(x,y) (x &= (~y))
#define FLIPBITMASK(x,y) (x ^= (y))
#define CHECKBITMASK(x,y) (x & (y))

void SMBus_Check(void);
void Set_StringParms(void);
void Phase_Adjust(void);
void Initialize_Vars(void);
void Calculate_Values(void);
void Timer_Count(void);
void Set_GooseTimer(void);
void Enter_SustainMode(void);
void Enter_StandbyMode(void);
void Duty_Cycle(void);

float sysclock = 20e06;												//system clock frequency (adjusted to calibrate)
float float_target_period = 0;										//temporary float value for target_period
float float_correct_period = 0;										//temporary float value for target_period
float float_temp = 0;
float phase_factor = 0;												//adjustments for phase delays (-0.45 > x > +0.45)
uint16_t target_period;												//counter overflow based on target frequency, clock dividers
uint8_t NOTE = DEFAULT_NOTE;
uint8_t divisor = 1;												//clock divisor
uint8_t avsamples = 1;												//number of samples to average and acceptable number of good cycles (smaller for low notes)
uint8_t irled_dc = 100;												//PWM duty cycle for sensor IRLED brightness 0 - 100 (set by PWM_CYCLE)
uint8_t timer100ms_count = 0;
uint8_t timer1s_count = 0;
uint8_t timer1s_last = 0;
bit crossover_mode = 0;												//special mode for sustainers placed in odd positions on strings
bit test_mode = 1;

//External Variables
uint16_t goosetime;													//target_period adjusted for PCA goose driver
uint16_t mingood;													//minimum allowable good value (+ 50 cents)
uint16_t maxgood;													//maximum allowable good value (- 50 cents)
uint16_t phase_delay = 0;											//phase shift between sensor input and coil output
uint8_t waves = 1;													//measure of multiple cycles for high treble
uint8_t multiplier = 1;												//period multiplier for output to master
uint8_t sustainer_addr = 2 * DEFAULT_NOTE;
uint8_t string_no = DEFAULT_STRING;									// set string (left 1, middle 2, right 3, off 0) // moved by MLC
volatile uint32_t period_cumulative = 0;							//sum for averaging period
volatile uint16_t period_avg = 0;									//average of timer counts for period
volatile uint8_t avcount = 0;										//counter for averaging
volatile uint16_t goodcount = 0;									//no. of consecutive good readings from string
volatile uint8_t wavecount = 1;										//count of waves for multiple-wave measurement, 1-4
volatile uint8_t calcount = 0;										//count of bad period measurements to trigger phase adjustment
volatile uint8_t dc_counter = 0;									//duty cycle counter
volatile uint16_t timer100us_count = 0;								//timer counters
bit debug_output = 1;
bit bipolar_mode = 1;
volatile bit coil_phase = 0;
volatile bit enable = 1;
volatile bit susmode = 0;											//0 = goose mode; 1 = phase delay capture mode; 2 = natural sustain mode

//state machine bits
bit initialized = 0;
bit calculated = 0;
bit goose_set = 0;
bit string_set = 0;

int main (void)
{
	enter_DefaultMode_from_RESET(); 								//call hardware initialization routine
	Enter_StandbyMode();
	//main program loop
	while (1)
	{
		if (!initialized)
			Initialize_Vars();
		if (!calculated)
			Calculate_Values();
		if (timer100us_count>=1000)
			Timer_Count();
		SMBus_Check();

		if(enable)
		{
			Duty_Cycle();
			if(!string_set)
				Set_StringParms();
			if (susmode)
			{
				if(avcount >= avsamples)									//averaging routine
				{
					period_avg = period_cumulative / avcount;				//average period_avg readings (in timer ticks)
					avcount = 0;											//zero count for next average
					period_cumulative = 0;									//zero total for next average
				}
			}
			else
			{
				if (!goose_set)
					Set_GooseTimer();										//start goose timer
				if(goodcount > avsamples)
					Enter_SustainMode();									//natural sustain mode
			}
			if(calcount > 5)							//increment phase delay guess for every 5 bad period_avg readings
				Phase_Adjust();
		}
		else //disabled
			Enter_StandbyMode();
	}
}

void Set_StringParms(void)
{
	switch(string_no) 								// P0.7 Left string setup
	{
		case LEFT:
		{
			P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED			//read appropriate sensor
					| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
					| P0MASK_B6__IGNORED | P0MASK_B7__COMPARED;

			P1MASK = P1MASK_B0__IGNORED | P1MASK_B1__IGNORED | P1MASK_B2__IGNORED
					| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
					| P1MASK_B6__IGNORED;
			break;
		}
		case MIDDLE: 								// P1.0 Middle string setup
		{
			P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED
					| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
					| P0MASK_B6__IGNORED | P0MASK_B7__IGNORED;

			P1MASK = P1MASK_B0__COMPARED | P1MASK_B1__IGNORED | P1MASK_B2__IGNORED
					| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
					| P1MASK_B6__IGNORED;
			break;
		}
		case RIGHT:									// P1.1 Right string setup
		{
			P0MASK = P0MASK_B0__IGNORED | P0MASK_B1__IGNORED | P0MASK_B2__IGNORED
					| P0MASK_B3__IGNORED | P0MASK_B4__IGNORED | P0MASK_B5__IGNORED
					| P0MASK_B6__IGNORED | P0MASK_B7__IGNORED;

			P1MASK = P1MASK_B0__IGNORED | P1MASK_B1__COMPARED | P1MASK_B2__IGNORED
					| P1MASK_B3__IGNORED | P1MASK_B4__IGNORED | P1MASK_B5__IGNORED
					| P1MASK_B6__IGNORED;
			break;
		}
		case  OFF:									//shut down sustainer, turn off coil and all LED's
		default:
		{
			Enter_StandbyMode();
			break;
		}
	}
	string_set = 1;
}

void Set_GooseTimer(void)
{
	susmode = 0;
	LED = OFF;
	PCA0H = goosetime >> 8;									//preset PCA goose driver
	PCA0L = goosetime;
	PCA0CN0_CF = 0;											//clear PCA overflow flag

	SETBITMASK(PCA0CN0, PCA0CN0_CR__BMASK);					//enable PCA timer (start goose)
	SETBITMASK(TCON, TCON_TR1__BMASK);						//start T1 (begin timing string periods)
	CLEARBITMASK(IE, IE_ET1__BMASK);						//disable T1 overflow interrupt during goose
	CLEARBITMASK(IE, IE_ET0__BMASK);						//disable T0 overflow interrupt during goose
	goose_set = 1;
}

void Enter_SustainMode(void)
{
	susmode = 1;
	goose_set = 0;
	LED = ON;
	SETBITMASK(IE, IE_ET1__BMASK);							//enable timer1 overflow interrupt for signal loss detection
	SETBITMASK(IE, IE_ET0__BMASK);							//enable timer0 overflow interrupt (phase delay)
	PCA0CN0_CR = 0;											//disable PCA counter (turn off goose)
	goodcount = 0;
}

void Enter_StandbyMode(void)
{
	CLEARBITMASK(PCA0CN0, PCA0CN0_CR__BMASK);	//stop goose
	CLEARBITMASK(IE, IE_ET1__BMASK);			//disable timer1 overflow interrupt
	CLEARBITMASK(IE, IE_ET0__BMASK);			//disable timer0 overflow interrupt
	P0MASK = 0;
	P1MASK = 0;
	LEFT_IR = OFF;
	MIDDLE_IR = OFF;
	RIGHT_IR = OFF;
	COIL_D1 = OFF;
	COIL_D2 = OFF;
	LED = OFF;
	susmode = 0;
	goose_set = 0;
	string_set = 0;
}

void Phase_Adjust(void)
{
	calcount = 0;							//clear bad period_avg counter
	goodcount = 0;							//clear good period_avg counter
	avcount = 0;							//zero current averaging count
	period_cumulative = 0;					//zero averaging total
	period_avg = 0;
	wavecount = 1;							//reset wave counter
	Set_GooseTimer();
	phase_factor += 0.05;					//phase increment
	if (phase_factor > 0.45)
		phase_factor = -0.45;				//phase roll over from +90deg to -90deg
	if(phase_factor < 0)
		phase_delay = 0xFFFF - floor(float_target_period * (phase_factor + 0.5));	//phase delay for timer0
	else
		phase_delay = 0xFFFF - floor(float_target_period * phase_factor);
}


void SMBus_Check(void)
{

	if(DATA_READY == 1)
	{
		DATA_READY = 0;

		switch(SMB_DATA_IN[0])
		{
		case 0:
			break;

		case 1:
			break;

		case 2:
			string_no = SMB_DATA_IN[1];
			string_set = 0;								//registers must be set accordingly
			if(string_no > 3)
				string_no = 0;

			test_mode = (SMB_DATA_IN[2] & 0xF0) >> 4;
			debug_output = test_mode;					//separate bit for debug output?
			enable = SMB_DATA_IN[2] & 0x0F;
			break;

		case 3:
			irled_dc = SMB_DATA_IN[1];
			break;

		case 4:
			break;

		case 9:
			initialized = 0;
			enable = 0;
			NOTE = SMB_DATA_IN[1];
			if (NOTE > 188)
				NOTE=188;
			if (NOTE<0)
				NOTE=0;
			sustainer_addr = 2*NOTE;
			break;
		default:
			break;
		}
	}
}

void Duty_Cycle(void)
{
	// duty cycle control for IR LEDs
	if (dc_counter>=irled_dc)
	{
		LEFT_IR = OFF;
		MIDDLE_IR = OFF;
		RIGHT_IR = OFF;
	}
	else
	{
		switch (string_no)
		{
			case LEFT:
			{
				LEFT_IR = ON;
				MIDDLE_IR = OFF;
				RIGHT_IR = OFF;
				break;
			}
			case MIDDLE:
			{
				LEFT_IR = OFF;
				MIDDLE_IR = ON;
				RIGHT_IR = OFF;
				break;
			}
			case RIGHT:
			{
				LEFT_IR = OFF;
				MIDDLE_IR = OFF;
				RIGHT_IR = ON;
				break;
			}
			default:
			{
				LEFT_IR = OFF;
				MIDDLE_IR = OFF;
				RIGHT_IR = OFF;
				break;
			}
		}
	}
}

void Initialize_Vars(void)
{
	calculated = 0;		//calculation phase must follow initialization

	//note presets
	if(NOTE > 100)													//convert crossover address to normal address and set mode
	{
		NOTE -= 100;
		crossover_mode = 1;
	}
	if(NOTE >= 43)													//notes above Eb4, full system clock speed, goose and measure
	{
		SETBITMASK(CKCON0, CKCON0_T1M__BMASK);					//full speed clock for timer1
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);				//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK);					//full speed clock for PCA goose timer
		divisor = 1;
		avsamples = 50;											//average low treble over 150 samples
	}
	if((NOTE < 43 && NOTE >= 19) || NOTE > 100)							//midrange notes between D2 and Eb4 (or crossover notes), divide system clock by 4
	{
		CLEARBITMASK(CKCON0, CKCON0_T1M__BMASK);				//timer1 prescaled
		CLEARBITMASK(CKCON0, CKCON0_T0M__BMASK);				//timer0 prescaled
		CLEARBITMASK(CKCON0, CKCON0_SCA__FMASK);				//clear SCA (timer1 clock select)
		SETBITMASK(CKCON0, CKCON0_SCA__SYSCLK_DIV_4);			//divide clock by 4 for timer1 & 2
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);				//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK_DIV_4);			//divide clock by 4 for PCA goose timer
		divisor = 4;
		avsamples = 25;											//average midrange over 50 samples
	}
	if(NOTE < 19)													//low notes below D2, divide system clock by 12
	{
		CLEARBITMASK(CKCON0, CKCON0_T1M__BMASK);				//timer1 prescaled
		CLEARBITMASK(CKCON0, CKCON0_T0M__BMASK);				//timer0 prescaled
		CLEARBITMASK(CKCON0, CKCON0_SCA__FMASK);				//clear SCA (timer1 select)
		SETBITMASK(CKCON0, CKCON0_SCA__SYSCLK_DIV_12);			//divide clock by 12 for timer1 & 2
		CLEARBITMASK(PCA0MD, PCA0MD_CPS__FMASK);				//clear CPS (PCA clock select)
		SETBITMASK(PCA0MD, PCA0MD_CPS__SYSCLK_DIV_12);			//divide clock by 12 for PCA goose timer
		divisor = 12;
		avsamples = 10;											//average bass over 15 samples
	}

	float_temp = NOTE;
	float_correct_period = sysclock / divisor / 27.5 / pow(2, float_temp/12);
	float_target_period = float_correct_period;
	if(test_mode)
		float_target_period = float_correct_period * .9;		//in test mode start of frequency sweep is above correct frequency

	initialized = 1;
}

void Calculate_Values(void)
{
	//initial calculations and presets
	target_period = floor(float_target_period + 0.5);			//target_period round to integer
	if(NOTE > 63)													//measure 4 complete waves for high treble (more accuracy)
	{
		waves = 4;
		target_period = floor(4 * float_target_period + 0.5);	//adjust correct period_avg for four waves
		avsamples = 5;											//average high treble over 500 samples
	}
	if(crossover_mode)											//read second harmonic for crossover notes (2f)
		target_period /= 2;
	maxgood = floor(target_period * 1.03 + 0.5);				//range for valid periods +/- 3% (50 cents)
	mingood = floor(target_period * .93 + 0.5);
	goosetime = 0xFFFF - (target_period / 2);					//proper period_avg value for goose (1/2 period_avg for toggle, count up to 0xFFFF overflow interrupt)
	if(waves == 4)
		goosetime = 0xFFFF - (target_period / 8);				//adjust goose frequency for multiple-wave measurement
	multiplier = divisor * waves;								//multiplier to convert period from timer ticks to sysclock ticks
	calculated = 1;
}

void Timer_Count(void)
{
	timer100us_count=0;
	timer100ms_count = timer100ms_count+1;
	if (timer100ms_count>=10)
	{
		timer100ms_count=0;
	}
	//Frequency sweep in test_mode
	if(test_mode && timer100ms_count==0 && goodcount < 2 && susmode==0 && COIL_D1 == 0)
	{
		calculated = 0;
		float_target_period = float_target_period *1.002;
		if (float_target_period >= float_correct_period*1.1)
			float_target_period = float_correct_period * .9;
	}
}
